package davmarc;

import java.util.ArrayList;
import java.util.Date;

public class Asignatura {
	//Una asignatura debe tener un nombre, una breve descripci�n, una
	//calificaci�n m�xima, una fecha de inicio y una fecha de fin.

	private String nombre;
	private String descripcion;
	private float calificacionMaxima;
	private Date fechaInicio;
	private Date fechaFin;
	
	public Asignatura(String nombre, String descripcion,
			float calificacionMaxima, Date fechaInicio, Date fechaFin,
			ArrayList<Periodo> periodos) {

		this.nombre = nombre;
		this.descripcion = descripcion;
		this.calificacionMaxima = calificacionMaxima;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.periodos = periodos;
		periodos = new ArrayList<>();
	}

	//Se desea que para una asignatura se puedan definir varios per�odos de
	//evaluaci�n. Como m�nimo existir� un per�odo con fecha de inicio y fin de la
	//propia de la asignatura. Cada per�odo de evaluaci�n estar� dado por una
	//fecha de inicio y una fecha de fin de per�odo.

	//Se desea que un per�odo lleve asociado un peso en la evaluaci�n total de la
	//asignatura que ser� tratado como un n�mero entre 0 y 1 representando el
	//porcentaje en que se considerar� el per�odo respecto al total de la
							//asignatura. La suma de los pesos de los per�odos definidos ser� exactamente	1
	ArrayList<Periodo> periodos;

	public void setPesoPeriodo(int periodo, float f) throws Exception{
		if(0f<=f && f<=1f){
			//pesoPeridodo.set(periodo, f);
		}
		else{
			throw new Exception("El peso ha de estar comprendido entre 0 y 1");
		}
	}
	
	//La suma de los pesos de las pruebas
	//en un per�odo no podr� ser mayor que 1. Pero no se exigir� que sea 1 para
	//dar margen a a�adir nuevas pruebas y otros elementos de evaluaci�n.
	
	//ArrayList<Prueba> pruebas;

	public void anadePrueba(Prueba p){
		if(sumaPesosPruebas() + p.getPeso() <= 1f){
			//pruebas.add(p);
		}
	}

	private float sumaPesosPruebas() {

		/*float suma = 0;
		
		for (int i = 0; i < pruebas.size(); i++) {

			suma += pruebas.get(i).getPeso();			
		}*/
		
		return suma;
	}
	
	//Para una asignatura podr� pedirse el listado de calificaciones de uno de los
	//per�odos o de la asignatura en total. Las calificaciones obtenidas por los
	//alumnos en la asignatura no podr�n ser mayores que la nota m�xima
	//asignada a esta. Se tendr� en cuenta que cada prueba tiene asociada una
	//calificaci�n m�xima que puede diferir una de otra y de la calificaci�n de la
	//asignatura.

	public ArrayList<Calificacion> getListadoCalificacionesPeriodo(Date fechaInicio, Date fechaFin){
		return null;
	}

	public ArrayList<Calificacion> getListadoCalificaciones(Date fechaInicio, Date fechaFin){
		return null;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public float getCalificacionMaxima() {
		return calificacionMaxima;
	}

	public void setCalificacionMaxima(float calificacionMaxima) {
		this.calificacionMaxima = calificacionMaxima;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public ArrayList<Periodo> getPeriodos() {
		return periodos;
	}

	public void setPeriodos(ArrayList<Periodo> periodos) {
		this.periodos = periodos;
	}

	public ArrayList<Prueba> getPruebas() {
		return pruebas;
	}

	public void setPruebas(ArrayList<Prueba> pruebas) {
		this.pruebas = pruebas;
	}
	
	
	
}
