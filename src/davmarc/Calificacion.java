package davmarc;

public class Calificacion {
	//Una calificaci�n de una prueba ser� un par <identificador de alumno, nota
	//obtenida en la prueba>. La calificaci�n no podr� ser mayor que la calificaci�n
	//m�xima indicada para la prueba. Se habilitar�n dos formas de a�adir
	//calificaciones a una prueba, a�adir una calificaci�n (se a�ade un par) o
	//a�adir un listado de calificaciones (de pares alumno-nota).
	
	public Calificacion(String id2, float nota2, float calificacionMaxima2) {
		id = id2;
		nota = nota2;
		calificacionMaxima = calificacionMaxima2;
	}
	
	private String id;
	private float nota;
	private float calificacionMaxima;
	
	
	public float getCalificacionMaxima() {
		return calificacionMaxima;
	}
	public void setCalificacionMaxima(float calificacionMaxima) {
		this.calificacionMaxima = calificacionMaxima;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public float getNota() {
		return nota;
	}
	public void setNota(float nota) {
		this.nota = nota;
	}

}
