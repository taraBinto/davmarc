package davmarc;

import java.util.ArrayList;
import java.util.Date;

public class Periodo {

	//Se desea que para una asignatura se puedan definir varios per�odos de
	//evaluaci�n. Como m�nimo existir� un per�odo con fecha de inicio y fin de la
	//propia de la asignatura. Cada per�odo de evaluaci�n estar� dado por una
	//fecha de inicio y una fecha de fin de per�odo.

	//Se desea que un per�odo lleve asociado un peso en la evaluaci�n total de la
	//asignatura que ser� tratado como un n�mero entre 0 y 1 representando el
	//porcentaje en que se considerar� el per�odo respecto al total de la
							//asignatura. La suma de los pesos de los per�odos definidos ser� exactamente	1

	private Date periodoFechaInicio;
	private Date periodoFechaFin;
	private Float pesoPeridodo;
	private ArrayList<Prueba> pruebas;
	
	public Periodo(Date periodoFechaInicio, Date periodoFechaFin,
			Float pesoPeridodo) {
		this.periodoFechaInicio = periodoFechaInicio;
		this.periodoFechaFin = periodoFechaFin;
		this.pesoPeridodo = pesoPeridodo;
		pruebas = new ArrayList<>();
	}

	public Date getPeriodoFechaInicio() {
		return periodoFechaInicio;
	}

	public void setPeriodoFechaInicio(Date periodoFechaInicio) {
		this.periodoFechaInicio = periodoFechaInicio;
	}

	public Date getPeriodoFechaFin() {
		return periodoFechaFin;
	}

	public void setPeriodoFechaFin(Date periodoFechaFin) {
		this.periodoFechaFin = periodoFechaFin;
	}

	public Float getPesoPeridodo() {
		return pesoPeridodo;
	}

	public void setPesoPeridodo(Float pesoPeridodo) {
		this.pesoPeridodo = pesoPeridodo;
	}

	
	
}
