package davmarc;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class Prueba {
	
	
	
	public Prueba(Date fechaDeRealizacion, String nombre, String descripcion,
			float calificacionMaxima, float pesoEvaluacion) {
		super();
		this.fechaDeRealizacion = fechaDeRealizacion;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.calificacionMaxima = calificacionMaxima;
		this.pesoEvaluacion = pesoEvaluacion;
	}

	//Vinculadas a la asignatura se podr�n crear pruebas. Las pruebas tendr�n
	//una fecha de realizaci�n. La fecha de realizaci�n de una prueba la enmarca
	//en uno de los per�odos de evaluaci�n.
	
	private Date fechaDeRealizacion;
	
	public boolean fechaEnmarcadaEnPeriodo(Date inicioPeriodo, Date finPeriodo){
		return true;
	}

	//Para cada prueba se indicar� adem�s un nombre, una breve descripci�n, una
	//calificaci�n m�xima y un peso en la evaluaci�n del per�odo correspondiente.
	//El peso ser� un n�mero entre 0 y 1 representando el porcentaje en que se
	//considerar� dicha nota en el per�odo. La suma de los pesos de las pruebas
	//en un per�odo no podr� ser mayor que 1. Pero no se exigir� que sea 1 para
	//dar margen a a�adir nuevas pruebas y otros elementos de evaluaci�n.
	
	private String nombre;
	private String descripcion;
	private float calificacionMaxima;
	private float pesoEvaluacion;
	

	public void setPeso(float f) throws Exception{
		if(0<=f && f<=1){
			pesoEvaluacion = f;
		}
		else{
			throw new Exception("El peso ha de estar comprendido entre 0 y 1");
		}
	}

	public float getPeso() {
		return pesoEvaluacion;
	}

	//La calificaci�n no podr� ser mayor que la calificaci�n m�xima indicada 
	//para la prueba. Se habilitar�n dos formas de a�adir
	//calificaciones a una prueba, a�adir una calificaci�n (se a�ade un par) o
	//a�adir un listado de calificaciones (de pares alumno-nota).
	
	ArrayList<Calificacion> calificaciones;

	//Se indicar� si una prueba ha sido calificada completamente. No se podr�n
	//a�adir nuevas calificaciones pero s� modificar calificaciones ya incluidas.
	public void a�adirCalificacion(Calificacion c){
		calificaciones.add(c);
	}

	public void a�adirListadoDeCalificaciones(ArrayList<Calificacion> calificacionesEntrantes){
		
		calificaciones.addAll(calificacionesEntrantes);
		
	}
	
	//Se indicar� si una prueba ha sido calificada completamente. No se podr�n
	//a�adir nuevas calificaciones pero s� modificar calificaciones ya incluidas.
	
	private boolean calificadaCompletamente;
	
	public void modificaCalificacion(String id, float calificacion){
		
	}
	
	//Para una prueba se podr� pedir el listado de calificaciones obtenidos (el par).
	//Si la fecha de la prueba no se ha celebrado, no podr� haberse calificado.

	public ArrayList<Calificacion> getListadoDeCalificacionesObtenidas(){
		return calificaciones;
	}

	public Date getFechaDeRealizacion() {
		return fechaDeRealizacion;
	}

	public void setFechaDeRealizacion(Date fechaDeRealizacion) {
		this.fechaDeRealizacion = fechaDeRealizacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public float getCalificacionMaxima() {
		return calificacionMaxima;
	}

	public void setCalificacionMaxima(float calificacionMaxima) {
		this.calificacionMaxima = calificacionMaxima;
	}

	public float getPesoEvaluacion() {
		return pesoEvaluacion;
	}

	public void setPesoEvaluacion(float pesoEvaluacion) {
		this.pesoEvaluacion = pesoEvaluacion;
	}

	public ArrayList<Calificacion> getCalificaciones() {
		return calificaciones;
	}

	public void setCalificaciones(ArrayList<Calificacion> calificaciones) {
		this.calificaciones = calificaciones;
	}

	public boolean isCalificadaCompletamente() {
		return calificadaCompletamente;
	}

	public void setCalificadaCompletamente(boolean calificadaCompletamente) {
		this.calificadaCompletamente = calificadaCompletamente;
	}
	
	
}
