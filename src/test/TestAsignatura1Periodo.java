package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import davmarc.Asignatura;
import davmarc.Periodo;
import davmarc.Prueba;

public class TestAsignatura1Periodo {

	Asignatura a;
	GregorianCalendar gc;
	ArrayList<Periodo> periodos;
	float calificacionMax;
	
	@Before
	public void setUp() throws Exception {
		
		gc = new GregorianCalendar();
		gc.add(GregorianCalendar.MONTH, -4);
		Date empiece = gc.getTime();
		gc.add(GregorianCalendar.MONTH, +5);
		Date fin = gc.getTime();
		gc.add(GregorianCalendar.MONTH, -1);

		periodos = new ArrayList<>();
		Periodo periodo1 = new Periodo(empiece, fin, 1.0f);
		
		periodos.add(periodo1);
		calificacionMax = 10.0f;
		
		a = new Asignatura("TDS", "Tecnologias para el Desarrollo de Software", calificacionMax, empiece, fin, periodos);
	}

	@Test
	public void test_anadePrueba() {
		
		Prueba p = new Prueba(gc.getTime(), "prueba1", "descripcion prueba 1", 10.0f, 20.0f);		
		a.anadePrueba(p);
		
		assertEquals("N� de pruebas anadidas incorrecto",a.getPruebas().size(),1);
		assertEquals("Mal anadidas pruebas",a.getPruebas().get(0), p);
	}

	@Test
	public void test_anadePrueba2() {
		
		Prueba p = new Prueba(gc.getTime(), "prueba1", "descripcion prueba 1", 10.0f, 20.0f);	
		Prueba p2 = new Prueba(gc.getTime(), "prueba2", "descripcion prueba 2", 10.0f, 77.0f);		
		a.anadePrueba(p);	
		a.anadePrueba(p2);
		
		assertEquals("N� de pruebas anadidas incorrecto",a.getPruebas().size(),2);
		assertEquals("Mal anadidas pruebas",a.getPruebas().get(0), p);
		assertEquals("Mal anadidas pruebas",a.getPruebas().get(1), p2);
	}

	@Test(expected = Exception.class)
	public void test_pruebaFueraPeriodoAntes() {

		GregorianCalendar gc2 = new GregorianCalendar();
		gc2.add(GregorianCalendar.MONTH, -10);
		
		Prueba p = new Prueba(gc2.getTime(), "prueba1", "descripcion prueba 1", 10.0f, 20.0f);		
		a.anadePrueba(p);		
	}

	@Test(expected = Exception.class)
	public void test_pruebaFueraPeriodoDespues() {

		GregorianCalendar gc2 = new GregorianCalendar();
		gc2.add(GregorianCalendar.MONTH, +10);
		
		Prueba p = new Prueba(gc2.getTime(), "prueba1", "descripcion prueba 1", 10.0f, 20.0f);		
		a.anadePrueba(p);		
	}
	
	@Test(expected = Exception.class)
	public void test_pruebaFueraPeriodoSumaPruebasIncorrecta() {		

		Prueba p = new Prueba(gc.getTime(), "prueba1", "descripcion prueba 1", 10.0f, 50.0f);	
		Prueba p2 = new Prueba(gc.getTime(), "prueba2", "descripcion prueba 2", 10.0f, 77.0f);		
		a.anadePrueba(p);	
		a.anadePrueba(p2);
	}

	@Test
	public void test_pruebaFueraPeriodoSumaPruebasCorrecta() {		

		Prueba p = new Prueba(gc.getTime(), "prueba1", "descripcion prueba 1", 10.0f, 50.0f);	
		Prueba p2 = new Prueba(gc.getTime(), "prueba2", "descripcion prueba 2", 10.0f, 50.0f);		
		a.anadePrueba(p);	
		a.anadePrueba(p2);
	}
	
	@Test
	public void test_pruebaFueraPeriodoSumaPruebasCorrecta2() {		

		Prueba p = new Prueba(gc.getTime(), "prueba1", "descripcion prueba 1", 10.0f, 30.0f);	
		Prueba p2 = new Prueba(gc.getTime(), "prueba2", "descripcion prueba 2", 10.0f, 40.0f);		
		a.anadePrueba(p);	
		a.anadePrueba(p2);
		
		ArrayList<Prueba> pruebas = a.getPruebas();
		float sumaPesosEvaluacion = 0.0f;
		
		
		for(int i = 0; i< pruebas.size(); i++){
			sumaPesosEvaluacion += pruebas.get(i).getPesoEvaluacion();
		}
		
		assertEquals("Mal sumados los pesos",sumaPesosEvaluacion, 70.0f, 0.0001f);
	}
	
	
	
	
	
}
