package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import davmarc.Calificacion;

public class TestCalificacion {

	Calificacion c;
	
	@Before
	public void setUp() throws Exception {
		c = new Calificacion("a1", 10.0f, 10.0f);
	}
	

	@Test(expected= Exception.class)
	public void testCreacionErronea() {
		c = new Calificacion("a2", 10.0f, 10.6f);
	}

	@Test(expected= Exception.class)
	public void testCreacionErronea2() {
		c = new Calificacion("a2", 10.0f, -10.6f);
	}

	@Test
	public void testCreacionCorrecta() {
		c = new Calificacion("a2", 10.0f, 6.2f);
		assertEquals("mal creada", c.getCalificacionMaxima(), 6.2f, 0.001f);
	}

	@Test
	public void testCreacionCorrecta2() {
		c = new Calificacion("a2", 10.0f, 10.0f);
		assertEquals("mal creada", c.getCalificacionMaxima(), 10.0f, 0.001f);
	}
	
}
