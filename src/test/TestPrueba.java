package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;

import org.junit.Before;
import org.junit.Test;

import davmarc.Calificacion;
import davmarc.Prueba;

public class TestPrueba {
	
	Prueba p;
	Calificacion c1;
	Calificacion c2;
	
	@Before
	public void setUp() throws Exception {
		
		GregorianCalendar gc = new GregorianCalendar();
		
		p = new Prueba(gc.getTime(), "nombre prueba", "descripcion prueba 1", 10.0f, 10.0f);
		
		c1 = new Calificacion("a1", 9.3f, 10.0f);
		c2 = new Calificacion("a2",7.3f, 11.0f);
	}

	@Test
	public void test_peso() throws Exception {		
		p.setPeso(0.30f);
		assertNotNull(p);
	}

	@Test(expected=Exception.class)
	public void test_pesoIncorrecto() throws Exception {		
		p.setPeso(-0.30f);
	}

	@Test(expected=Exception.class)
	public void test_pesoIncorrecto2() throws Exception {		
		p.setPeso(1.30f);
	}

	@Test
	public void test_calificacionMaxima() throws Exception {		
		p.setCalificacionMaxima(2.4f);
		assertEquals("maximo incorrecto", p.getCalificacionMaxima(), 2.4f, 0.01);
	}

	@Test
	public void test_calificacionMaxima2(){	
		p.setCalificacionMaxima(11.0f);
		p.aņadirCalificacion(c1);
		assertNotNull(p);
	}

	@Test(expected=Exception.class)
	public void test_calificacionMaximaIncorrecta() throws Exception {		
		p.setCalificacionMaxima(2.4f);
		p.aņadirCalificacion(c1);
	}

	@Test
	public void test_aņadirListado() {	
		
		ArrayList<Calificacion> arc = new ArrayList<>();
		arc.add(c1);
		arc.add(c2);

		p.aņadirListadoDeCalificaciones(arc);
		assertNotNull(p);
		assertEquals("arrays diferentes", arc, p.getCalificaciones());
	}
	
	
	
}
